# Sharebuttons

## What's this?
This is a sloppy script that I wrote to add share buttons on my websites.

Privacy-oriented and minimalistic.

It requies Javascript to work while no php support or external assets are required.

## License
This script is distributed under the CC0 license (which is virtually same as public domain but with more legal stability). Therefore you can freely and handily use or redistribute this script.

## Supported services
- Mastodon
- Pocket
- Tumblr
- Hatena bookmark
- Twitter
- Facebook
- LINE

## How to use
1. Put sharebuttons.js to somewhere on your server. Let's say "~/src/js/".
2. Add `<script defer type="text/javascript" src="/src/js/sharebuttons.js"></script>` and `<div id="sharebuttons"></div>` to your webpages.
3. It (ought to) work.

## Decoration
You may want to decorate the buttons with CSS. This is a simple example:

```
.sharebutton {
	cursor: pointer;
	color: #0645ad;
	display: inline-block;
	border: 1px solid #c0c0c0;
	padding: 6px;
	margin: 3px;
	border-radius: 3px;
}
```

If you plan to use icons, read their trademark policies carefully. They usually require to follow specific style (such as shape or color).
