// Sharebuttons by maruyuki
// https://gitlab.com/maruyuki/sharebuttons

const shareButtons = document.getElementById('sharebuttons');
const shareUrl = encodeURIComponent(location.href);
const shareText = document.title;

generateShareButtons(shareButtons, shareUrl, shareText);

function generateShareButtons(area, url, text) {
    const mdBtn = document.createElement('span');
    mdBtn.className = 'mastodon-btn';
    const pkBtn = document.createElement('span');
    pkBtn.className = 'pocket-btn';
    const tbBtn = document.createElement('span');
    tbBtn.className = 'tumblr-btn';
    const htBtn = document.createElement('span');
    htBtn.className = 'hatena-btn';
    const twBtn = document.createElement('span');
    twBtn.className = 'twitter-btn';
    const fbBtn = document.createElement('span');
    fbBtn.className = 'facebook-btn';
    const liBtn = document.createElement('span');
    liBtn.className = 'line-btn';

    const pkHref = 'https://widgets.getpocket.com/v1/popup?url='+shareUrl;
    const tbHref = 'https://www.tumblr.com/widgets/share/tool?posttype=link&title='+shareText+'&canonicalUrl='+shareUrl;
    const htHref = 'https://b.hatena.ne.jp/entry/'+shareUrl;
    const twHref = 'https://twitter.com/share?text='+shareText+'&url='+shareUrl;
    const fbHref = 'https://www.facebook.com/share.php?u='+shareUrl;
    const liHref = 'https://line.me/R/msg/text/?'+shareText+' '+shareUrl;

    const clickEvl = 'onClick="openWindow(this.href); return false;"';
    const mdLink = '<span id="mdbtn" class="sharebutton" onClick="clickMastodon(shareUrl, shareText)">Mastodon</span>';
    const pkLink = '<a href="' + pkHref + '" ' + clickEvl + ' id="pkbtn" class="sharebutton" rel="nofollow noopener noreferrer">Pocket</a>';
    const tbLink = '<a href="' + tbHref + '" ' + clickEvl + ' id="tbbtn" class="sharebutton" rel="nofollow noopener noreferrer">Tumblr</a>';
    const htLink = '<a href="' + htHref + '" ' + clickEvl + ' id="htbtn" class="sharebutton" rel="nofollow noopener noreferrer">Hatena Bookmark</a>';
    const twLink = '<a href="' + twHref + '" ' + clickEvl + ' id="twbtn" class="sharebutton" rel="nofollow noopener noreferrer">Twitter</a>';
    const fbLink = '<a href="' + fbHref + '" ' + clickEvl + ' id="fbbtn" class="sharebutton" rel="nofollow noopener noreferrer">Facebook</a>';
    const liLink = '<a href="' + liHref + '" target="_blank" id="libtn" class="sharebutton" rel="nofollow noopener noreferrer">LINE</a>';

    mdBtn.innerHTML = mdLink;
    pkBtn.innerHTML = pkLink;
    tbBtn.innerHTML = tbLink;
    htBtn.innerHTML = htLink;
    twBtn.innerHTML = twLink;
    fbBtn.innerHTML = fbLink;
    liBtn.innerHTML = liLink;

    area.appendChild(mdBtn);
    area.appendChild(pkBtn);
    area.appendChild(tbBtn);
    area.appendChild(htBtn);
    area.appendChild(twBtn);
    area.appendChild(fbBtn);
    area.appendChild(liBtn);
}

function clickMastodon(url, text) {
    const mastodonInstanceUrl = window.prompt("Mastodon Instance URL?", "fedibird.com");
    if (mastodonInstanceUrl !== null) {
        const mtHref = 'https://'+encodeURIComponent(mastodonInstanceUrl)+'/share?text='+shareText+'&url='+shareUrl;
        openWindow(mtHref);
        return false;
    }
}

function openWindow(url) {
    window.open(url, "noreferrer", 'width=800, height=400, menubar=no, toolbar=no, scrollbars=yes');
}
